package com.spark.mllibext.regression

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import com.spark.mllibext.matrixutils.PseudoInverse
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.rdd.RDD

/**
  * Created by sunil.mamidi on 3/3/17.
  *
  *==overview==
  * A distributed technique to solve Linear Regression on RDD data
  *
  */

object StandardLinearRegression {

	val logger = Logger(LoggerFactory.getLogger(this.getClass))

	val debugSampleLog:Int=1


	/**
	  *  <br>Trains a linear regression model  Y = \phi(X) + \epsilon on distributed RDD dataset
	  *  <br>W = (X'X)-1X'Y = PINV*Y = (Y'*PINV')'
	  *  <br>PINV' = [[com.spark.mllibext.matrixutils.PseudoInverse#computeRightInvTranpose]].
	  *  PINV' is calculated using inplace techniques and doesn't shuffle the rows
	  *  <br>Y'*PINV' is calcuated by zipWithIndex and join operation and then using partition
	  *  aggregate and reduction technique
	  *
	  * @param trainx : input X vector  or \phi(X) with basis functions
	  * @param traint : target Y vector , accepts single dimension Y for now
	  * @param columnSize : dimension of X vector
	  * @return : model W parameters
	  */

	def train(trainx: RDD[Vector], traint: RDD[Double], columnSize:Int) : Array[Double] = {

		val pseudoRowMatTranspose =  PseudoInverse.computeRightInvTranpose(new RowMatrix(trainx))

		val pinvT_zip = pseudoRowMatTranspose.rows.zipWithIndex.map{case (vec:Vector, ind:Long) => (ind,vec)}
		val traint_zip = traint.zipWithIndex.map{case (x:Double,y:Long) => (y,x)}

		logger.debug(" train target \n " +traint_zip.take(debugSampleLog)
			.map{ case (x:Long, y:Double) => x.toString+" "+y.toString }.mkString("\n"))

		logger.debug(" pinv zipIndex \n" +pinvT_zip.take(debugSampleLog)
			.map{ case (x:Long, v:Vector) => x.toString+" "+v.toString}.mkString("\n"))


		val weightedPseudoRowMat  = pinvT_zip.join(traint_zip)
			.map{ case (x:Long, (y:org.apache.spark.mllib.linalg.Vector, z:Double)) => y.toArray.map{k=>k*z}}

		logger.debug("Weighted PseduoRowMat size "+ weightedPseudoRowMat.count()+" \n" +
			weightedPseudoRowMat.take(debugSampleLog)
			.map{case x:Array[Double] => x.map{k=>k.toString}.mkString(",")}.mkString("\n"))


		val model1 =  weightedPseudoRowMat.mapPartitions(it => Iterator(it
			.aggregate(new Array[Double](columnSize))
			((U,v) => U.zip(v).map{case(x,y) => x+y},(U1,U2) => U1.zip(U2)
				.map{ case(x,y) => x+y} ))).reduce((U1,U2) => U1.zip(U2).map{case(x,y)=> x+y})
		model1

	}
	
}
