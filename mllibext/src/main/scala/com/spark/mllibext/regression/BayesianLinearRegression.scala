package com.spark.mllibext.regression

import breeze.linalg.{inv, DenseMatrix => BDM, DenseVector => BDV}
import com.typesafe.scalalogging.Logger
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.{DenseMatrix, DenseVector, Vector, Vectors}
import org.apache.spark.rdd.RDD
import org.slf4j.LoggerFactory


/**
  * Created by sunil.mamidi on 3/4/17.
  *
  * ==overview==
  * <br>trains a bayesian regression model on distributed RDD, idea is to avoid shuffling of data and use
  * inplace techniques
  *
  * <br>model complexity analysis and  estimate hyperparameters in progress
  */
object BayesianLinearRegression {

	val logger = Logger(LoggerFactory.getLogger(this.getClass))

	var posterior_mean : DenseVector = _
	var posterior_covariance : DenseMatrix = _

	/**
	  *<br> posteriorMean = inv(SN)*(beta*\phi'*t)
	  *<br> SN = inv(inv(s0) + beta*phi'*phi)
	  *<br> CORRECTION : beta is noise in measurement, should be just scalar.
	  * @param trainX : train X data , raw values or transformed with basis functions \phi
	  * @param trainY : train Y data
	  * @param columnSize : dimension of input X
	  * @param alpha : model (W) prior variance . prior is assumed to be normal distribution with mean = 0 and variance = alpha
	  * @param beta :  variance (error) of Y given x. Y= W*\phi(X) + \gaussian(0,\beta).
	  * @return : model parameters
	  */

	def train( trainX:RowMatrix, trainY:RDD[Double],columnSize:Int , alpha:Double, beta:Array[Double]) : (DenseVector,DenseMatrix)={


		val beta_eyemat = DenseMatrix.diag(Vectors.dense(beta))

		val betaPhiTPhi = trainX.computeGramianMatrix().multiply(beta_eyemat)
		val breezeMat =  new BDM(betaPhiTPhi.numRows, betaPhiTPhi.numCols, betaPhiTPhi.values)

		val SN = inv(breezeMat)
		val SN_dense = new DenseMatrix(SN.rows,SN.cols,SN.data)

		val trainX_Ind = trainX.rows.zipWithIndex.map{case (x:Vector, y:Long) => (y,x) }
		val trainY_Ind = trainY.zipWithIndex.map{case (x:Double, y:Long) => (y,x)}
		val trainX_trainY_join = trainX_Ind.join(trainY_Ind).map{case ( ind:Long, (x:Vector, y:Double) ) => x.toArray.map{k=>k*y}}

		val trainX_trainY_join_reduce =   trainX_trainY_join.reduce((U1,U2) => U1.zip(U2).map{ case(x,y) => x+y})


		val trainX_trainY_join_beta = trainX_trainY_join_reduce.zip(beta).map{ case (x,y) => x*y}

		posterior_mean = SN_dense.multiply( Vectors.dense(trainX_trainY_join_beta))

		posterior_covariance = SN_dense

		(posterior_mean, posterior_covariance)

	}

	/**
	  * <br> predicts the Y values by marginalizing likelihood with posterior w distribution
	  * <br> // p(y/(w,\alpha,\beta)) = \Integral(p(y/xtest,w,\beta)*p(w/(xtrain,ytrain\alpha))dw)
	  * @param testx : test data in RDD format
	  * @param posteriorWSamples : model posterior samples
	  * @param columnSize : input data dimension
	  * @return  test Y values
	  */
	def predictTest(testx:RDD[Vector], posteriorWSamples:Array[Array[Double]], columnSize:Int ) = {

		// for marginalizatoin with respect to W, add Wn from all posterior samples and divide by weight
		val posteriorWeightSum = posteriorWSamples.transpose.map{_.sum}

		logger.info("posterior weight sum " + posteriorWeightSum.toList.toString)

		val ypredict = testx.map{ case (x: Vector) => {

			x.toArray.zipWithIndex.map{case (a:Double,b:Int) => a*posteriorWeightSum(b)/posteriorWSamples.size}.sum

		}
		}
		ypredict
	}



	/**
	  * <br> predicts the Y values by marginalizing likelihood with posterior w distribution
	  * <br> // p(y/(w,\alpha,\beta)) = \Integral(p(y/xtest,w,\beta)*p(w/(xtrain,ytrain\alpha))dw)
	  * @param testx : testdata in array format
	  * @param posteriorWSamples : model posterior samples
	  * @param columnSize : input X dimension
	  * @return : test Y values
	  */
	def predictTest(testx:Array[Array[Double]], posteriorWSamples:Array[Array[Double]], columnSize:Int ) = {

		// for marginalizatoin with respect to W, add Wn from all posterior samples and divide by weight
		val posteriorWeightSum = posteriorWSamples.transpose.map{_.sum}.map{x=>x/posteriorWSamples.size}

		logger.info("posterior weight sum " + posteriorWeightSum.toList.toString)

		val ypredict = testx.map{ case (x: Array[Double]) => {
			x.zipWithIndex.map{case (a:Double,b:Int) => a*posteriorWeightSum(b)}.sum

		}
		}
		ypredict
	}

}
