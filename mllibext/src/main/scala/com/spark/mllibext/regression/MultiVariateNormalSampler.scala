package com.spark.mllibext.regression

import breeze.linalg.{DenseMatrix => BDM,upperTriangular}
import com.github.fommil.netlib.LAPACK.{getInstance => lapack}
import com.typesafe.scalalogging.Logger
import org.apache.spark.mllib.linalg.{DenseMatrix, DenseVector}
import org.slf4j.LoggerFactory

/**
  * Created by sunil.mamidi on 3/3/17.
  *
  * ==overview==
  *
  * Random Sampling from a multivariate Normal distribution
  */
object MultiVariateNormalSampler {

	val logger = Logger(LoggerFactory.getLogger(this.getClass))


	/**<br> Randomly draws sample from multivariate normal distribution.
	  *<br> The technique is described here
	  * [[https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Drawing_values_from_the_distribution]]
	  *<br> First get LDL decomposition of covariance matrix ( A= LL' OR U'U ) using lapack
	  *<br> second use a set of k seeds to generate vector samples from normal distribution with mean =0 variance=1
	  *<br> Finally obtain samples using formula  S = \mu + Lz or S = \mu + Uz
	  *<br>
	  * @param multiVariateMean : mean vector of the multivariate normal distribution \mu
	  * @param multiVariateCovariance : covariance matrix A
	  * @param numOfSamples : no of samples
	  * @return array of  sample vectors from the distribution
	  */
	def getSamples(multiVariateMean:DenseVector, multiVariateCovariance:DenseMatrix, numOfSamples:Int) :Array[Array[Double]] ={

		val randMultiVariate:Array[scala.util.Random] = new Array[scala.util.Random](multiVariateMean.size)


		// use  k set of randomseeds accordingly for k dimension of the input data set ,to generate randomobjects for z vector
		for( i <- 0 to multiVariateMean.size-1) {
			randMultiVariate(i) = new scala.util.Random(scala.util.Random.nextInt(multiVariateMean.size))
		}

		val z = new Array[Array[Double]](numOfSamples)
		for( i <-0 to numOfSamples-1) {

			var tempArray= new Array[Double](0)
			randMultiVariate.foreach{x=> {tempArray = tempArray :+ x.nextGaussian()}}
			z(i) = tempArray
		}

		var posteriorCovariance_array :Array[Double]   = multiVariateCovariance.toArray

		lapack.dpotrf("U",multiVariateMean.size,posteriorCovariance_array,multiVariateMean.size,new org.netlib.util.intW(0))
		logger.debug("done with lU decomp "+posteriorCovariance_array(0))

		val LDL_Upper =  upperTriangular(new BDM(multiVariateMean.size, multiVariateMean.size, multiVariateCovariance.toArray))
		logger.debug("done with upper traingle" +LDL_Upper.toArray(0))
		val S =  z.map{ case (arr:Array[Double]) =>  {
			val ldlarr:breeze.linalg.DenseVector[Double] = LDL_Upper*breeze.linalg.DenseVector(arr)
			ldlarr.toArray.zipWithIndex.map{case (k:Double,ind:Int) => k+multiVariateMean.toArray(ind)}
		}}
		//.zip(posteriorMean).map{case (a:Double,b:Double) => a+b}
		logger.debug("done with Samples "+S(0)(0))
		S
	}
	
}
