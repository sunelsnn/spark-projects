package com.spark.mllibext.matrixutils

import breeze.linalg.{inv, DenseMatrix => BDM, DenseVector => BDV}
import org.apache.spark.mllib.linalg.DenseMatrix
import org.apache.spark.mllib.linalg.distributed.RowMatrix


/**
  * Created by sunil.mamidi on 3/3/17.
  *==overview==
  * distributed way to compute pseudoinverse of a skinnytall matrix with rows distributed as RDD
  */
object PseudoInverse {


	/*
	def  computeWeightedRightInvTranspose(dataRowMat :RowMatrix, weightVector:DenseVector) :RowMatrix ={
	} */


	/**
	  *<br>Computes PseudoInverse with out shuffling rows. The return matrix is Tranpose of pinv
	  *<br>inv(A'A)A' = ((A(inv(A'A))')' = (A*inv(Grammat)')'
	  *<br>grammat=A'A is calculated using inplace rank 1 updates by spark routines
	  *<br>then datamatrix A is multiplied by inv(grammat)'
	  *
	  *@param dataRowMat : data RowMatrix
	  *@return : returns a skinnytall PseudoInversemat
	  *
	 **/
	def computeRightInvTranpose (dataRowMat : RowMatrix) : RowMatrix =  {

		val grammat : DenseMatrix = dataRowMat.computeGramianMatrix().asInstanceOf[DenseMatrix]

		val gmb = new BDM(grammat.numRows, grammat.numCols, grammat.values)

		val grammatInv = inv(gmb).t

		val invRowMat = dataRowMat.multiply(new DenseMatrix(grammatInv.rows,grammatInv.cols,grammatInv.data))

		invRowMat

	}

	
}
