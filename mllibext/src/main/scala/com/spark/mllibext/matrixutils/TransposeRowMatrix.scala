package com.spark.mllibext.matrixutils

import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.{Vector, Vectors}

/**
  * Created by sunil.mamidi on 3/3/17.
  *
  * computes Transpose of a RowMatrix
  */
object TransposeRowMatrix {


	/**
	  * <br> Transposes a RowMatrix , works for moderately sized  Matrices.
	  * <br> will not work for RowMatrix with a several billion Rows  spread across several nodes.
	  * <br> each node may not have enough capacity to house the single vertical column of original matrix
	  * @param m : RowMatrix RDD
 	  * @return RowMatrix RDD
	  */
	def transposeRowMatrix(m: RowMatrix): RowMatrix = {
		lazy val transposedRowsRDD = m.rows.zipWithIndex
			.map{ case (row, rowIndex) =>   rowToTransposedTriplet(row, rowIndex)}
			.flatMap(x=>x)
			.groupByKey
			.sortBy{case (i,ax) => i}.map(_._2)
			.map(buildRow)
		new RowMatrix(transposedRowsRDD)
	}


	// def mapfunc = lambda (row,rowIndex) : rowToTransposedTriplet(row,rowIndex)
	def mapfunct(row : Vector, rowIndex : Long) : TraversableOnce[(Long , (Long, Double))] = {
		rowToTransposedTriplet(row, rowIndex)
	}


	def rowToTransposedTriplet(row: Vector, rowIndex: Long): Array[(Long, (Long, Double))] = {
		val indexedRow = row.toArray.zipWithIndex
		indexedRow.map{case (value, colIndex) => (colIndex.toLong, (rowIndex, value.toDouble))}
	}

	def buildRow(rowWithIndexes: Iterable[(Long, Double)]): Vector = {
		val resArr = new Array[Double](rowWithIndexes.size)
		rowWithIndexes.foreach{case (index, value) =>
			resArr(index.toInt) = value
		}
		Vectors.dense(resArr)
	}

	def printRowMat(rmat : RowMatrix) = {
		rmat.rows.collect().foreach(println)
	}
	
}
