package com.spark.mllibext.matrixutils

import breeze.linalg.{DenseMatrix => BDM}
import org.apache.spark.mllib.linalg.distributed.RowMatrix

/**
  * Created by sunil.mamidi on 3/3/17.
  *
  * ==overview==
  *
  * Breeze Utils
  */

object BreezeUtil {

	/**
	  * convert RowMatrix to breezeMatrix, works for small matrices
	  * @param rowMatrix
	  * @return breeze DenseMatrix
	  */
	def toBreeze(rowMatrix : RowMatrix): BDM[Double] = {
		val m = rowMatrix.numRows().toInt
		val n = rowMatrix.numCols().toInt
		val mat = breeze.linalg.DenseMatrix.zeros[Double](m, n)
		var i = 0
		rowMatrix.rows.collect().foreach { vector =>
			vector.foreachActive { case (j, v) =>
				mat(i, j) = v
			}
			i += 1
		}
		mat
	}
}
