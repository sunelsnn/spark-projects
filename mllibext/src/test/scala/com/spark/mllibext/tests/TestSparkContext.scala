package com.spark.mllibext.tests

import java.util.Properties

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by sunil.mamidi on 3/4/17.
  */
object TestSparkContext {

	//jars=/Users/sunil.mamidi/gitrepos/spark-projects/mllibext/target/mllib-ext-1.0-SNAPSHOT.jar:/Users/sunil.mamidi/gitrepos/spark-projects/mllibext/target/mllib-ext-1.0-SNAPSHOT-tests.jar

	var sc:SparkContext = null
	def getSparkContext(appName:String) = {

		if (sc!=null){
			sc
		} else {

			val mavenProperties = new Properties()
			mavenProperties.load(this.getClass.getClassLoader.getResourceAsStream("mllibext_maven.properties"))

			val prop = new Properties()
			prop.load(this.getClass.getClassLoader.getResourceAsStream("sparkcontext.prop"))

			val conf = new SparkConf().setAppName(appName)
				.setMaster(prop.getProperty("master"))

			prop.keySet().toArray().map { x => x.asInstanceOf[String] }
				.foreach { k => conf.set(k, prop.getProperty(k)) }

			sc = new SparkContext(conf)
			// add test and source jars
			sc.addJar(mavenProperties.getProperty("mllibext.project.basedir")+"/"+"target/"
				+mavenProperties.getProperty("mllibext.artifact")+"-"
				+mavenProperties.getProperty("mllibext.version")+".jar")
			sc.addJar(mavenProperties.getProperty("mllibext.project.basedir")+"/"+"target/"
				+mavenProperties.getProperty("mllibext.artifact")+"-"
				+mavenProperties.getProperty("mllibext.version")+"-tests.jar")

			if(prop.getProperty("jars")!=null) {
				prop.getProperty("jars").split(":").foreach { x => sc.addJar(x) }
			}

			sc
		}
	}
	
}
