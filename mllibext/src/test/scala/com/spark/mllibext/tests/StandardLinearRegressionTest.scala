package com.spark.mllibext.tests

import com.spark.mllibext.regression.StandardLinearRegression
import org.apache.spark.SparkContext
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.util.Random

/**
  * Created by sunil.mamidi on 3/4/17.
  */
class StandardLinearRegressionTest extends FunSuite with BeforeAndAfterAll{


	var sc: SparkContext = null

	override def beforeAll(): Unit = {
		sc = TestSparkContext.getSparkContext("StandardLinearTest")

		}

	/** solve linear equation Y = 3*x1+7*x2
	  *
	  */
	test("standard LinearRegression") {

		val train_nsample = 10000
		val test_nsample = 1000
		val maxsample = 100
		val x_vec = new Array[org.apache.spark.mllib.linalg.Vector](train_nsample)

		val y_val = new Array[Double](train_nsample)


		for (i <- 0 to train_nsample-1) {

			x_vec(i) = org.apache.spark.mllib.linalg.Vectors.dense(Random.nextDouble , Random.nextDouble  )
			y_val(i) = (3*x_vec(i).apply(0) ) + (7*x_vec(i).apply(1) )

		}
		val mod = StandardLinearRegression.train(sc.parallelize(x_vec), sc.parallelize(y_val), 2)

		// assert that the coefficients difference beless than 0.001
		assert(Math.abs(mod(0)-3) < 0.001)
		assert(Math.abs(mod(1)-7) < 0.001)
	}


	
}
