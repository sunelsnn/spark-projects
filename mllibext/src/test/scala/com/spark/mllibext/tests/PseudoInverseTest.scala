package com.spark.mllibext.tests


import breeze.linalg.{DenseMatrix => BDM, DenseVector => BDV}
import breeze.numerics._
import com.spark.mllibext.matrixutils.{PseudoInverse, TransposeRowMatrix}
import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.{DenseMatrix, DenseVector, Vector, Vectors}
import org.apache.spark.rdd.RDD
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import org.slf4j.LoggerFactory

/**
  * Created by sunil.mamidi on 3/4/17.
  *
  * ==overview==
  * test the distributedInverse for various datasets
  */

class PseudoInverseTest extends FunSuite with BeforeAndAfterAll {

	var sc: SparkContext = null
	val logger = Logger(LoggerFactory.getLogger("PINV"))

	override def beforeAll(): Unit = {

		logger.info("executing beforeall")

		sc = TestSparkContext.getSparkContext("PseudoInverseTest")

	}

	/**
	  * <br>create a matrix of 1000*5 of randomInts and find the pseudoInverse.
	  * <br>test that pseduoinverse when multiplied by original matrix is eye . (i.e) eye = PINV.x
	  */
	test(" distributed pseudo inverse test ") {

		val arrVec = new Array[Vector](1000)
		for(i <- 0 to 999) {
			arrVec(i) = Vectors.dense((scala.util.Random.nextInt(10000)+1), (scala.util.Random.nextInt(10000)+1),
				(scala.util.Random.nextInt(10000)+1),(scala.util.Random.nextInt(10000)+1),
				(scala.util.Random.nextInt(10000)+1))

		}

		val rowm = new RowMatrix(sc.parallelize(arrVec))

		val invmat = TransposeRowMatrix.transposeRowMatrix(PseudoInverse.computeRightInvTranpose(rowm))

		val rowmT = TransposeRowMatrix.transposeRowMatrix(rowm)

		val dm = new DenseMatrix(rowmT.numCols().asInstanceOf[Int], rowmT.numRows().asInstanceOf[Int], rowmT.rows.flatMap(vec => vec.toArray).collect(), false)


		val expectedID = invmat.multiply(dm)
		println(" rows " +expectedID.numRows() + " cols  "+expectedID.numCols())
		expectedID.rows.collect().take(1).foreach(println)

		// test expectedID is identity matrix
		val diffarr = expectedID.rows.zipWithIndex.flatMap{ case(vec:Vector,ind:Long) => {
			val arr = vec.toArray
			println(arr.toList)
			val arr2: Array[Double] = Array.fill(arr.size)(0);
			arr2(ind.toInt) = 1.0
			arr.zipWithIndex.map { case (value1: Double, index1: Int) => {
				if (abs(value1 - arr2(index1)) <= 0.1) 0 else 1
			}
			}
		}
		}.sum

		assert(diffarr==0)

	}

	test("pseudoinverse hdfs test") {

		val textfileRDD = sc.textFile("hdfs://localhost/user/sunil.mamidi/pseduomatinv")

		val vectorRDD: RDD[Vector] = textfileRDD.map { x => {
			val arrd: Array[Double] = x.split(",").map { y => y.toDouble }
			new DenseVector(arrd)
		}
		}
		val rmat: RowMatrix = new RowMatrix(vectorRDD)

		val invmat = TransposeRowMatrix.transposeRowMatrix(PseudoInverse.computeRightInvTranpose(rmat))

		val rmatT = TransposeRowMatrix.transposeRowMatrix(rmat)

		val dm: DenseMatrix = new DenseMatrix(rmatT.numCols().asInstanceOf[Int], rmatT.numRows().asInstanceOf[Int], rmatT.rows.flatMap(vec => vec.toArray).collect(), false)

		val expectedIdentity = invmat.multiply(dm)

		val diffsum = expectedIdentity.rows.zipWithIndex().flatMap { case (vec: Vector, indX: Long) => {
			val arrD = vec.toArray;
			arrD.zipWithIndex.map { case (vald: Double, indY: Int) => if (indX == indY) vald - 1 else vald }
		}
		}
			.map { kval => if (abs(kval) < 0.1) 0 else 1 }.sum()

		assert(diffsum == 0)


	}



}