package com.spark.mllibext.tests

import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import org.slf4j.LoggerFactory
import com.spark.mllibext.regression.{BayesianLinearRegression, MultiVariateNormalSampler}

import scala.util.Random

/**
  * Created by sunil.mamidi on 3/4/17.
  */
class BayesianLinearRegressionTest extends FunSuite with BeforeAndAfterAll {

	var sc: SparkContext = null

	val logger = Logger(LoggerFactory.getLogger(this.getClass))

	override def beforeAll(): Unit = {

		logger.info("executing beforeall")

		sc = TestSparkContext.getSparkContext("BayesRegressTest")

	}

	/**
	  * test bayes regression on linear plane
	  */
	test("bayes regress ") {

		val train_nsample = 100000
		val test_nsample = 10
		val maxsample = 100
		val x_vec = new Array[org.apache.spark.mllib.linalg.Vector](train_nsample)
		val y_val = new Array[Double](train_nsample)
		val x_test_vec = new Array[Array[Double]](test_nsample)
		val y_test_val = new Array[Double](test_nsample)


		for (i <- 0 to train_nsample - 1) {
			x_vec(i) = org.apache.spark.mllib.linalg.Vectors.dense(Random.nextDouble, Random.nextDouble)
			y_val(i) = (3 * x_vec(i).apply(0)) + (7 * x_vec(i).apply(1))
		}

		val y_rdd = sc.parallelize(y_val)
		val x_rowmat = new RowMatrix(sc.parallelize(x_vec))
		val (pos_mean, pos_covariance) = BayesianLinearRegression.train(x_rowmat, y_rdd, 2, 0.01, Array(0.01, 0.01))

		logger.info("pos mean "+pos_mean.values(0) + " " + pos_mean.values(1))
		logger.info("pos covariance" +  pos_covariance.toString)



		for (i <- 0 to test_nsample - 1) {
			x_test_vec(i) = Array(Random.nextDouble, Random.nextDouble)
			y_test_val(i) = (3 * x_test_vec(i).apply(0)) + (7 * x_test_vec(i).apply(1))


		}

		val pos_sample = MultiVariateNormalSampler.getSamples(pos_mean, pos_covariance, 10)

		pos_sample.foreach(x=> println(x.toList.toString()))

		val y_test_pred = BayesianLinearRegression.predictTest(x_test_vec,
			MultiVariateNormalSampler.getSamples(pos_mean, pos_covariance, 1000),
			2)


		assert(Math.abs(pos_mean.values(0)-3) < 0.01)
		assert(Math.abs(pos_mean.values(1)-7) < 0.01)

		for(i <- 0 to test_nsample-1) {
		 assert(Math.abs(y_test_pred(i)-y_test_val(i))<1)
		}


	}

	// input X= gaussian distribution \phi(x) = 3*cos^2(x(0)) +7*cos^2(x(1))
	// fit is worse since R2 < 0
	test("bayes regress test a sinusoidal basis function with gaussian variable") {

		val train_nsample = 100000
		val test_nsample = 10
		val maxsample = 100
		val beta = 0.01
		val alpha = 0.01
		val x_vec = new Array[org.apache.spark.mllib.linalg.Vector](train_nsample)
		val y_val = new Array[Double](train_nsample)
		val x_test_vec = new Array[Array[Double]](test_nsample)
		val y_test_val = new Array[Double](test_nsample)

		// x_vals are radians and y_val = asin(x_val)

		for (i <- 0 to train_nsample - 1) {

			x_vec(i) = org.apache.spark.mllib.linalg.Vectors.dense(Math.cos(Random.nextGaussian()), Math.cos(Random.nextGaussian()))
			y_val(i) = (3 * x_vec(i).apply(0) * x_vec(i).apply(0)) + (7 * x_vec(i).apply(1) * x_vec(i).apply(1))
			+scala.util.Random.nextGaussian() * beta


		}
		val y_rdd = sc.parallelize(y_val)
		val x_rowmat = new RowMatrix(sc.parallelize(x_vec))
		val (pos_mean, pos_covariance) = BayesianLinearRegression.train(x_rowmat, y_rdd, 2, alpha, Array(beta, beta))


		logger.info(" pos mean "+pos_mean.values(0) + " " + pos_mean.values(1))
		logger.info(" pos covariance " + pos_covariance.toString)

		for (i <- 0 to test_nsample - 1) {

			x_test_vec(i) = Array(Math.cos(Random.nextGaussian()), Math.cos(Random.nextGaussian()))
			y_test_val(i) = (3 * x_test_vec(i).apply(0) * x_test_vec(i).apply(0)) + (7 * x_test_vec(i).apply(1) * x_test_vec(i).apply(1)) + scala.util.Random.nextGaussian() * beta

		}

		val y_test_pred = BayesianLinearRegression.predictTest(x_test_vec,
			MultiVariateNormalSampler.getSamples(pos_mean, pos_covariance, 1000),
			2)


		logger.debug(" ytestpred " + y_test_pred.length + " " + y_test_pred(0) + " " + y_test_pred(1))

		var y_test_mean: Double = y_test_val.sum / test_nsample
		var y_SSE: Double = 0
		var y_deviation: Double = 0

		// R^2 and RMSE
		for (i <- 0 to test_nsample - 1) {
			logger.debug(" x " + x_test_vec(i).apply(0) + " " + x_test_vec(i).apply(1) + " " + y_test_val(i) + " " + y_test_pred(i))

			y_SSE += (y_test_val(i) - y_test_pred(i)) * (y_test_val(i) - y_test_pred(i))
			y_deviation += (y_test_val(i) - y_test_mean) * (y_test_val(i) - y_test_mean)

		}

		val R2 = 1 - (y_SSE / y_deviation)

		val RMSE = Math.sqrt(y_SSE / test_nsample)

		logger.info(" R2 and RMSE " + R2 + " " + RMSE)

		// assert R2 is not super bad  i.e fitness indicator. generally negative R2 implies fitness is bad
		assert(R2 > -0.1)

	}

	// input X= uniform distribution \phi(x) = 3*cos^2(x(0)) +7*cos^2(x(1))
	// fit is good R2 > 0.8
	test("bayes regress test a sinusoidal basis function with uniform distribution") {

		val train_nsample = 100000
		val test_nsample = 1000
		val maxsample = 100
		val beta = 0.01
		val alpha = 0.01
		val x_vec = new Array[org.apache.spark.mllib.linalg.Vector](train_nsample)
		val y_val = new Array[Double](train_nsample)
		val x_test_vec = new Array[Array[Double]](test_nsample)
		val y_test_val = new Array[Double](test_nsample)


		// generate train data
		for (i <- 0 to train_nsample - 1) {
			x_vec(i) = org.apache.spark.mllib.linalg.Vectors.dense(Math.cos(Random.nextDouble()),
				Math.cos(Random.nextDouble()))
			y_val(i) = (3 * x_vec(i).apply(0) * x_vec(i).apply(0)) + (7 * x_vec(i).apply(1) * x_vec(i).apply(1))
			+scala.util.Random.nextGaussian() * beta


		}

		// train the input data
		val y_rdd = sc.parallelize(y_val)
		val x_rowmat = new RowMatrix(sc.parallelize(x_vec))
		val (pos_mean, pos_covariance) = BayesianLinearRegression.train(x_rowmat, y_rdd, 2, alpha, Array(beta, beta))


		logger.info(" posterior mean W " + pos_mean.values(0) + " " + pos_mean.values(1))
		logger.info("pos covariance "+pos_covariance.toString)

		// generate TestData
		for (i <- 0 to test_nsample - 1) {
			x_test_vec(i) = Array(Math.cos(Random.nextDouble()), Math.cos(Random.nextDouble()))
			y_test_val(i) = (3 * x_test_vec(i).apply(0) * x_test_vec(i).apply(0)) +
				(7 * x_test_vec(i).apply(1) * x_test_vec(i).apply(1)) +
				scala.util.Random.nextGaussian() * beta

		}

		// predict test data
		val y_test_pred = BayesianLinearRegression.predictTest(x_test_vec,
			MultiVariateNormalSampler.getSamples(pos_mean, pos_covariance, 1000),2)


		// check fitness , compute R^2 and RMSE
		var y_test_mean: Double = y_test_val.sum / test_nsample
		var y_SSE: Double = 0
		var y_deviation: Double = 0

		for (i <- 0 to test_nsample - 1) {
			logger.debug("view few samples")
			logger.debug(" x " + x_test_vec(i).apply(0) + " " + x_test_vec(i).apply(1) +
				" " + y_test_val(i) + " " + y_test_pred(i))

			y_SSE += (y_test_val(i) - y_test_pred(i)) * (y_test_val(i) - y_test_pred(i))
			y_deviation += (y_test_val(i) - y_test_mean) * (y_test_val(i) - y_test_mean)

		}

		val R2 = 1 - (y_SSE / y_deviation)
		val RMSE = Math.sqrt(y_SSE / test_nsample)

		logger.info(" R2 and RMSE " + R2 + " " + RMSE)

		assert(RMSE < 2.0)
		assert(R2 > 0.5)

	}


}
