# Spark Projects

## MLLIB Extensions ##
    1.  ###  Matrix Utils ###
        1. Distributed PseduoInverse
        2. Transpose Row Matrix
        3. Breeze Util

    2. ### Linear Regression ###
        1. Standard Linear Regression
        2. Bayesian Linear Regression



## Docker Tests ##
        This project uses [docker-maven-plugin](https://github.com/fabric8io/docker-maven-plugin) ,  which starts spark container with a master and a worker
        Simple way to run docker tests in your project directory execute following.
        some test related hdfs may fail , since the work on setting up hadoop-yarn container is in progress.
        (you can specific test that interests you). In the end kill your background process

        1) mvn docker:build
        2) nohup mvn docker:run &
        3) mvn test
        4) mvn docker:stop
        5) kill -9 $(ps -ef | grep 'docker:run' | awk -F" " '{print $2}')