mvn docker:build
nohup mvn docker:run &
mvn test
mvn docker:stop
kill -9 $(ps -ef | grep 'docker:run' | awk -F" " '{print $2}')
